#!/usr/bin/env python
# vim: set fileencoding=utf-8 :

import re
import bleach
from bs4 import BeautifulSoup, Tag

tags = [
    'a', 'p', 'b', 'strong', 'i', 'em', 'u', 'div', 'img', 'table', 'tr', 'td',
    'thead', 'tbody', 'tfoot', 'th', 'q', 'blockquote', 'ol', 'ul', 'li',
    'sub', 'sup'
] + ['h{0}'.format(i) for i in range(1, 7)]

attrs = {
    'a': ['href', 'title'],
    'img': ['src'],
    'td': ['colspan', 'rowspan'],
    'th': ['colspan', 'rowspan']
}

strange_space = re.compile(ur'\u00a0')
empty_tags = re.compile(ur'<(p|div|h[1-6]|span|b|u|i|a|strong|em)>(\s|\u00a0)*</\1>')
useless_divs = re.compile(ur'^</?div>$', re.MULTILINE)
single_word_lines = re.compile(ur'((?:<(?:p|b|h\d)>)+)([\w ·•–,.0-9-():]{,30})\n')
bad_captions = re.compile(ur'([a-z]{0,3}-ima-(?:\d+-)?) (\d+)\ ?', re.IGNORECASE)
neighbour_tags = re.compile(ur'</([a-z]+)><\1>')
multiple_nls = re.compile(u'\n\n\n+')

def fix(handle):
    # Use BeautifulSoup so we do not have to care about encoding issues
    doc = BeautifulSoup(handle)

    # Bleach the output to get rid of bad MS shit
    out = bleach.clean(doc.body, tags, attrs, strip=True)

    # Replace U+00a0 NO-BREAK SPACE with ' '
    out = strange_space.sub('', out)

    # Remove empty tags
    changed = True
    while changed:
        stripped = empty_tags.sub('', out)
        changed = out != stripped
        out = stripped

    # Remove </foo><foo>
    out = neighbour_tags.sub('', out)

    # Remove <div> tags on their own line
    out = useless_divs.sub('', out)

    # Join <p>Single\n word paragrahps to the next line
    out = single_word_lines.sub(ur'\1\2 ', out)

    # Fix badly spaced captions (ABC-IMA-201- 1 Caption)
    out = bad_captions.sub(ur'\1\2 ', out)

    # Compress large sets of \n\n\n to \n\n
    out = multiple_nls.sub('\n\n', out)

    # Add target='_blank' to all links, or remove empty ones
    doc = BeautifulSoup(out)
    for anchor in doc.find_all('a'):
        if anchor.has_attr('href'):
            anchor['target'] = '_blank'
        else:
            parent = anchor.parent
            index = parent.contents.index(anchor)
            anchor.extract()
            for i, child in enumerate(anchor.contents):
                parent.insert(index + i, child)

    # Find the first node child. If it is a table, delete it
    for child in doc.body.children:
        if isinstance(child, Tag):
            if child.name == 'table':
                child.extract()
            break

    body_str = unicode(doc.body)
    inner_str = body_str[6:-7]

    return inner_str


def wrap(body):
    bits = ["""<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="../common/style.css">
<script src="../common/APIWrapper.js"></script>
<script>
	function Init() {
		doInitialize();
		doSetValue("cmi.completion_status", "completed");
		doTerminate();
	}
</script>
<title></title>
</head>
<body onload="Init();">
""", body, """
</body>
</html>"""]
    return ''.join(bits)

