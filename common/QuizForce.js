function WriteSwfObject(strSwfFile, nWidth, nHeight, strScale, strAlign, strQuality, strBgColor, strFlashVars) {
	var strHtml = "";

	strHtml += "<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' codebase='http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,79,0' id='player' width='" + nWidth + "' height='" + nHeight + "' align='" + strAlign + "'>";
	strHtml += "<param name='allowScriptAccess' value='sameDomain' />";
	strHtml += "<param name='scale' value='" + strScale + "' />";
	strHtml += "<param name='movie' value='" + strSwfFile + "' />";
	strHtml += "<param name='quality' value='" + strQuality + "' />";
	strHtml += "<param name='bgcolor' value='" + strBgColor + "' />";
	strHtml += "<param name='flashvars' value='" + strFlashVars + "' />";
	strHtml += "<param name='salign' value='lt' />";
	strHtml += "<embed src='" + strSwfFile +"' name='player' flashvars='" + strFlashVars + "' scale='" + strScale + "' quality='" + strQuality + "' bgcolor='" + strBgColor + "' width='" + nWidth + "' height='" + nHeight + "' align='" + strAlign + "' allowscriptaccess='sameDomain' type='application/x-shockwave-flash' pluginspage='http://www.macromedia.com/go/getflashplayer' />";
	strHtml += "</object>";

	document.write(strHtml);
}

function CloseWindow() {
	window.close();
}

function player_DoFSCommand(command, args) {

	args = String(args);
	command = String(command);

	args = args.replace(/&amp;/g,"&");
	args = args.replace(/&quot;/g,"\"");
	args = args.replace(/&apos;/g,"'");
	var arrArgs = args.split("|~|");

	switch (command) {
		case "QF_StatusAndScores":

			doSetValue("cmi.success_status", arrArgs[7]);
		doSetValue("cmi.score.max", arrArgs[8]);
		doSetValue("cmi.score.raw", arrArgs[9]);
		var scale = parseInt(arrArgs[9], 10) / parseInt(arrArgs[8], 10);
		doSetValue("cmi.score.scaled", "" + scale);

		var strDescription = arrArgs[0];
		var strId = arrArgs[1];
		var strType = arrArgs[2];
		var strCorrectResponse = arrArgs[3];
		var strStudentResponse = arrArgs[4];
		var strResult = arrArgs[5];
		var strWeight = arrArgs[6];
		var strStudent = "";
		var strCorrect = "";
		var i;

		switch (strType) {
		case "choice":
			strStudent = strStudentResponse.replace("|#|", "[,]");
			strCorrect = strCorrectResponse.replace("|#|", "[,]");
			break;
		case "fill-in":
			strCorrect = strCorrectResponse.replace("|#|", "[,]");
			break;
		case "matching":
			var arrUserResult = strStudentResponse.split("|#|");
			for (i = 0; i < arrUserResult.length; i++)
			arrUserResult[i] = "" + (i+1) + arrUserResult[i];
			strStudent = arrUserResult.join("[,]");

			var arrCorrectResult = strCorrectResponse.split("|#|");
			for (i = 0; i < arrCorrectResult.length; i++)
			arrCorrectResult[i] = "" + (i+1) + arrCorrectResult[i];
			strCorrect = arrCorrectResult.join("[,]");
			break;
		case "sequencing":
			strStudent = strStudentResponse.replace("|#|", "[,]");
			strCorrect = strCorrectResponse.replace("|#|", "[,]");
			break;
		case "performance":
			break;
		case "likert":
			break;
		}
		RecordInteraction(strId, strStudent, strResult, strCorrect, strDescription, parseInt(strWeight, 10), strType);
		break;
		case "QF_CloseAndExit":
			doSetValue( "adl.nav.request", "exitAll" );
		doSetValue( "cmi.completion_status", "completed" );
		doTerminate();
		break;
	}
}

function thisMovie(movieName) {
	// IE and Netscape refer to the movie object differently for our ongoing pleasure.
	//usage: thisMovie('player').SetVariable('_root.mcMyNotes.vMySlideNotes',  str );
	if (navigator.appName.indexOf ("Microsoft") !=-1) {
		return window[movieName];
	} else {
		return document[movieName];
	}
}
