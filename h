#!/bin/bash

in=$( cat )

hn="h$1"
text="$( echo "${in}" | sed 's/<[^>]*>//g' | grep . )"

echo "<$hn>$text</$hn>"
